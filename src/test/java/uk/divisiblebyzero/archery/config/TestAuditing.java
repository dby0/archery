package uk.divisiblebyzero.archery.config;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.context.annotation.Profile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import uk.divisiblebyzero.archery.ArcheryApplication;
import uk.divisiblebyzero.archery.domain.SightMark;
import uk.divisiblebyzero.archery.service.SightMarkRepository;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by: Matthew Smalley
 * Date: 17/09/2016
 */
@Profile("unit")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(ArcheryApplication.class)
@WebIntegrationTest
public class TestAuditing {
    @Resource
    private SightMarkRepository sightMarkRepository;

    @Before
    public void before() {
        sightMarkRepository.deleteAll();
        SightMark sightMark = new SightMark();
        sightMark.setDistance(100);
        sightMarkRepository.save(sightMark);
    }

    @Test
    @WithMockUser
    public void testSaveSightMark() {
        List<SightMark> sightMarks = sightMarkRepository.findAll();
        assertTrue(sightMarks.size() > 0);
        assertNotNull(sightMarks.get(0).getUserId());
    }
}
