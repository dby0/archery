package uk.divisiblebyzero.archery.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by: Matthew Smalley
 * Date: 22/11/2015
 */
@Profile("unit")
@Configuration
@WebAppConfiguration
public class UnitTestConfig extends SpringConfig {
/*    @Bean
    @Override
    public MongoClient mongo() {
        return new Fongo(getDatabaseName()).getMongo();
    }*/
}
