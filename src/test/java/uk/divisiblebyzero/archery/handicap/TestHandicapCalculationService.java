package uk.divisiblebyzero.archery.handicap;

import org.junit.Assert;
import org.junit.Test;
import uk.divisiblebyzero.archery.domain.Round;

/**
 * Created by: Matthew Smalley
 * Date: 08/09/2016
 */
public class TestHandicapCalculationService {
    private HandicapCalculationService handicapCalculationService = new HandicapCalculationService();

    @Test
    public void testHandicapCalc() {
        Assert.assertEquals(57, handicapCalculationService.getHandicapForScore(Round.NATIONAL, 350));
        Assert.assertEquals(50, handicapCalculationService.getHandicapForScore(Round.NATIONAL, 440));

        Assert.assertEquals(49, handicapCalculationService.getHandicapForScore(Round.PORTSMOUTH, 490));
        Assert.assertEquals(40, handicapCalculationService.getHandicapForScore(Round.PORTSMOUTH, 530));

        Assert.assertEquals(63, handicapCalculationService.getHandicapForScore(Round.FROSTBITE, 187));

        System.out.println(handicapCalculationService.getHandicapTable(Round.WORCESTER));

        Assert.assertEquals(69, handicapCalculationService.getHandicapForScore(Round.WORCESTER, 100));
    }
}
