package uk.divisiblebyzero.archery.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import uk.divisiblebyzero.archery.ArcheryApplication;
import uk.divisiblebyzero.archery.domain.ArcheryRound;
import uk.divisiblebyzero.archery.domain.Dozen;

import javax.annotation.Resource;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by: Matthew Smalley
 * Date: 22/11/2015
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(ArcheryApplication.class)
@WebIntegrationTest
public class TestArcheryRoundService {

    @Resource
    private ArcheryRoundService archeryRoundService;
    @Resource
    private DataGenerator dataGenerator;

    @Test
    public void testCalculateTotalsMismatchedEnds() {
        ArcheryRound a = dataGenerator.getSampleArcheryRound();
        a.getDozens().get(0).setEnd1(null);
        archeryRoundService.calculateTotals(a);
    }

    @Test
    public void testCalculateTotals() {
        ArcheryRound a = dataGenerator.getSampleArcheryRound();
        archeryRoundService.calculateTotals(a);

        //9, 9, 9, 8, 7, 6
        int i = 0;
        for (Dozen d : a.getDozens()) {
            i++;
            assertEquals(0, d.getGolds());
            assertEquals(12, d.getHits());
            assertEquals(48, d.getEnd0().getEndTotal());
            assertEquals(48, d.getEnd1().getEndTotal());
            assertEquals(96, d.getScore());
            assertEquals(96 * i, d.getRunningTotal());
        }
        assertEquals(96 * i, a.getTotalScore());
    }
}
