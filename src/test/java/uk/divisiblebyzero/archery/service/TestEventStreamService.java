package uk.divisiblebyzero.archery.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import uk.divisiblebyzero.archery.ArcheryApplication;
import uk.divisiblebyzero.archery.domain.ArcheryEvent;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 23/01/2016
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(ArcheryApplication.class)
@WebIntegrationTest
public class TestEventStreamService {
    private final static Log LOG = LogFactory.getLog(TestEventStreamService.class);
    @Resource
    private EventStreamService eventStreamService;

    @Test
    public void testGetStream() {
        List<ArcheryEvent> eventStream = eventStreamService.getEvents();
        LOG.info(eventStream);
    }
}
