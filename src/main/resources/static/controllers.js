    var archeryControllers = angular.module('archeryControllers', ["xeditable", "ui.bootstrap"]);
    archeryControllers.run(function(editableOptions) {
      editableOptions.theme = 'bs3';
    });

    archeryControllers.controller("ArcheryScoresCtrl", function($scope, $http, $log, $location) {

      $scope.sortType = "date";
      $scope.sortReverse = false;
      $scope.getArcheryRounds = function() {
        $http.get("archeryRounds").then(function(response) {
          $scope.archeryRounds = response.data._embedded.archeryRounds;
        }, function(response) {
          if (response.status == 403) {
            $scope.status = "Access denied";
          } else {
            $scope.status = "Unknown error downloading data";
          }
        });
      }

      $scope.getEvents = function() {
        $http.get("events").then(function(response) {
          $scope.archeryEvents = response.data;
        }, function(response) {
          if (response.status == 403) {
            $scope.status = "Access denied";
          } else {
            $scope.status = "Unknown error downloading data";
          }
        });
      }


      $scope.recalculateRounds = function() {
        $http.get("recalculate").then(function(response) {
          $scope.downloadState();
          $scope.status = "Recalculated";
        }, function(response) {
          if (response.status == 403) {
            $scope.status = "Access denied";
          } else {
            $scope.status = "Unknown error recalculating details";
          }
        });
      }

      $scope.downloadState = function() {
          $scope.getEvents();
      };


      $scope.pageName = "scores";

      $scope.newArcheryRound = function() {
        $location.path("/scores/0");
      }

      $scope.showDetails = function(id) {
        $location.path("/scores/" + id);
      };

      $scope.showArcher = function(archer) {
        $location.path("/archer/" + archer);
      };

      $scope.downloadState();

    });

    archeryControllers.controller("SightMarksCtrl", function($scope, $http, $routeParams, $log) {
      $scope.getSightMarks = function() {
        $http.get("sightMarks/").success(function(response) {
          $scope.sightMarks = response._embedded.sightMarks;
          $log.log(JSON.stringify($scope.sightMarks));
          $scope.newSightMark = { timestamp: Date.now() };
          $scope.detailsDirty = false;
        });
      };

      $scope.setModified = function() {
        $scope.detailsDirty = true;
      };

        $scope.saveSightMark = function() {
          $http.post("sightMarks", $scope.newSightMark).then(function(response) {
            $scope.archeryRoundId = response.data.id;
            $scope.getSightMarks();
            $scope.status = "Saved";
          }, function(response) {
            if (response.status == 403) {
              $scope.status = "Access denied";
            } else {
              $scope.status = "Unknown error saving details";
            }
          });
        };


      $scope.getSightMarks();
    });

    archeryControllers.controller("ArcheryRoundDetailsCtrl", function($scope, $http, $log, $routeParams) {

      $scope.archeryRoundId = $routeParams.archeryRoundId;
      $scope.getArcheryRoundDetails = function() {
        $http.get("archeryRounds/" + $scope.archeryRoundId).success(function(response) {
          $scope.archeryRoundDetails = response;
          $scope.detailsDirty = false;
        });
      }

      $scope.getArcheryRoundDetails();

      $scope.setModified = function() {
        $scope.detailsDirty = true;
      };

      $scope.saveDetails = function() {
        $http.post("archeryRounds", $scope.archeryRoundDetails).then(function(response) {
          $scope.archeryRoundId = response.data.id;
          $scope.getArcheryRoundDetails();
          $scope.status = "Saved";
        }, function(response) {
          if (response.status == 403) {
            $scope.status = "Access denied";
          } else {
            $scope.status = "Unknown error saving details";
          }
        });
      };

      $scope.bowTypes = [{
        value: 'RECURVE',
        text: 'Recurve'
      }, {
        value: 'COMPOUND',
        text: 'Compound'
      }, {
        value: 'BAREBOW',
        text: 'Barebow'
      }, {
        value: 'LONGBOW',
        text: 'Longbow'
      }];

      $scope.roundTypes = [{
        value: 'PORTSMOUTH',
        text: 'Portsmouth'
      }, {
        value: 'FROSTBITE',
        text: 'Frostbite'
      }];

      $scope.addDozen = function() {
        if ($scope.archeryRoundDetails.dozens == null) {
          $scope.archeryRoundDetails.dozens = [];
        }
        $scope.archeryRoundDetails.dozens.push({
          "end0": {
            "scores": [0, 0, 0, 0, 0, 0]
          },
          "end1": {
            "scores": [0, 0, 0, 0, 0, 0]
          }
        });
      };

    });

    archeryControllers.controller("ArcherDetailsCtrl", function($scope, $http, $routeParams, $log) {
        $scope.archer = $routeParams.archer;

        $scope.roundTypes = {};
        $scope.roundTypes["FROSTBITE"] = {name: "Frostbite", range: [175,175]};
        $scope.roundTypes["PORTSMOUTH"] = {name: "Portsmouth", range: [400,400]};
        $scope.roundTypes["WORCESTER"] = {name: "Worcester", range: [100,100]};
        $scope.roundTypes["FITA18"] = {name: "FITA 18", range: [350,350]};

        $http.get("/graphdata?archer=" + $scope.archer).then(function(response) {
            $scope.scoreData = response.data;
            $log.log(JSON.stringify($scope.scoreData));
        });

        $scope.range = function(roundType) {
            if (roundTypes[roundType]) {
                return $scope.roundTypes[roundType].range;
            } else {
                return null;
            }
        }

        $scope.xAxisTickFormatFunction = function(){
            return function(d){
                return d3.time.format('%x')(new Date(d));
            }
        }
    });

    archeryControllers.controller("NavBarCtrl", function($scope, $location, $http) {
      $scope.path = $location.path();
      $http.get("/user").success(function(data) {
        $scope.user = data.userAuthentication.details.displayName;
        $scope.authenticated = true;
      }).error(function() {
        $scope.user = "N/A";
        $scope.authenticated = false;
      });
      $scope.logout = function() {
        $http.post('/logout', {}).success(function() {
          $scope.authenticated = false;
          $location.path("/");
        }).error(function(data) {
          console.log("Logout failed")
          $scope.authenticated = false;
        });
      };
      $scope.loginWithGoogle = function() {
        $location.href = "login/google";
      }
    });
