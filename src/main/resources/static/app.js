var archeryApp = angular.module('archeryApp', [
  'ngRoute',
  'archeryControllers',
  'nvd3ChartDirectives'
]);


archeryApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/scores', {
        templateUrl: 'partial.scores.html',
        controller: 'ArcheryScoresCtrl'
      }).
      when('/scores/:archeryRoundId', {
        templateUrl: 'partial.rounddetail.html',
        controller: 'ArcheryRoundDetailsCtrl'
      }).
      when('/archer/:archer', {
        templateUrl: 'partial.archerdetail.html',
        controller: 'ArcherDetailsCtrl'
      }).
      when('/sightMarks', {
        templateUrl: 'partial.sightMarks.html',
        controller: 'SightMarksCtrl'
      }).
      otherwise({
        redirectTo: '/scores'
      });
  }]);