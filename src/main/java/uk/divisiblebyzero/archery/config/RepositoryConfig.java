package uk.divisiblebyzero.archery.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import uk.divisiblebyzero.archery.domain.ArcheryRound;
import uk.divisiblebyzero.archery.domain.SightMark;

/**
 * Created by: Matthew Smalley
 * Date: 17/01/2016
 */
@Configuration
public class RepositoryConfig {

    @Bean
    public RepositoryRestConfigurer repositoryRestConfigurer() {

        return new RepositoryRestConfigurerAdapter() {
            @Override
            public void configureRepositoryRestConfiguration(
                    RepositoryRestConfiguration config) {
                config.exposeIdsFor(ArcheryRound.class, SightMark.class);
            }
        };
    }
}