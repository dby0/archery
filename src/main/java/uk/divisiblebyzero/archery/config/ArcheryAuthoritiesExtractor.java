package uk.divisiblebyzero.archery.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.AuthoritiesExtractor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import uk.divisiblebyzero.archery.domain.User;
import uk.divisiblebyzero.archery.service.UserRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by: Matthew Smalley
 * Date: 18/01/2016
 */
@Component
public class ArcheryAuthoritiesExtractor implements AuthoritiesExtractor {
    @Autowired
    private UserRepository userRepository;

    @Override
    public List<GrantedAuthority> extractAuthorities(Map<String, Object> map) {
        List<Map> emails = (List<Map>) map.get("emails");
        String email = "" + emails.get(0).get("value");
        System.out.println("Looking for user: " + email);
        System.out.println("user repository is: " + userRepository);
        User user = userRepository.findByEmail(email);
        if (user != null) {
            return AuthorityUtils.commaSeparatedStringToAuthorityList(asAuthorities(user.getRoles()));
        } else {
            return new ArrayList<>();
        }
    }

    private String asAuthorities(Object object) {
        if (object instanceof Collection) {
            return StringUtils.collectionToCommaDelimitedString((Collection<?>) object);
        }
        if (ObjectUtils.isArray(object)) {
            return StringUtils.arrayToCommaDelimitedString((Object[]) object);
        }
        return object.toString();
    }

}

