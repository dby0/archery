package uk.divisiblebyzero.archery.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

/**
 * Created by: Matthew Smalley
 * Date: 17/09/2016
 */
@Configuration
@EnableMongoAuditing
public class MongoAuditing {
    @Bean
    public AuditorAware<String> myAuditorProvider() {
        return new AuditorAware<String>() {
            @Override
            public String getCurrentAuditor() {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

                if (authentication == null) {
                    return null;
                }

                if (authentication instanceof OAuth2Authentication) {
                    OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) authentication;
                    String name = oAuth2Authentication.getUserAuthentication().getName();
                    return name;
                }
                if (authentication.isAuthenticated()) {
                    String currentUserName = authentication.getName();
                    return currentUserName;
                }
                return null;
            }
        };
    }
}
