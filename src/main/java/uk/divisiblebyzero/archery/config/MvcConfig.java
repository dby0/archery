package uk.divisiblebyzero.archery.config;

/**
 * Created by: Matthew Smalley
 * Date: 17/01/2016
 */

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addRedirectViewController("/archery", "/archery.html");
        registry.addRedirectViewController("/", "/archery.html");
/*
        registry.addViewController("/").setViewName("archery");
        registry.addViewController("/login").setViewName("login");
*/
    }

}