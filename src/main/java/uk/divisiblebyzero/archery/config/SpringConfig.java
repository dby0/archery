package uk.divisiblebyzero.archery.config;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.WriteConcern;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * Created by: Matthew Smalley
 * Date: 17/11/2015
 */
/*
@Configuration
@ComponentScan(basePackageClasses = {ServicePackage.class, SpringConfig.class})
@PropertySource("classpath:/archery.properties")
*/
public class SpringConfig extends AbstractMongoConfiguration {

    @Value("${archery.db.name:archery}")
    private String databaseName;

    @Value("${archery.db.url:mongodb://localhost}")
    private String url;

    @Value("${OPENSHIFT_MONGODB_DB_URL:}")
    private String openShiftMongoDbUrl;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    // ---------------------------------------------------- MongoTemplate

    @Override
    protected String getDatabaseName() {
        return databaseName;
    }

    @Override
    @Bean
    public MongoClient mongo() throws Exception {
        if (openShiftMongoDbUrl != null && !"".equals(openShiftMongoDbUrl)) {
            url = openShiftMongoDbUrl;
        }
        MongoClient client = new MongoClient(new MongoClientURI(url));
        client.setWriteConcern(WriteConcern.SAFE);
        return client;
    }

    @Override
    protected String getMappingBasePackage() {
        return "uk.divisionbyzero.archery.domain";
    }

    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(mongo(), getDatabaseName());
    }
}

