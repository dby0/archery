package uk.divisiblebyzero.archery.domain;

/**
 * Created by: Matthew Smalley
 * Date: 08/09/2016
 */
public enum ScoringStyle {
    IMPERIAL(9), METRIC(10), WORCESTER(5);
    private int goldScore;

    ScoringStyle(int goldScore) {
        this.goldScore = goldScore;
    }

    public int getGoldScore() {
        return goldScore;
    }

    public void setGoldScore(int goldScore) {
        this.goldScore = goldScore;
    }
}
