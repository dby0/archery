package uk.divisiblebyzero.archery.domain;

/**
 * Created by: Matthew Smalley
 * Date: 28/03/2016
 */
public class Conversions {

    /**
     * Factor to convert yards into metres
     */
    public final static double YARDS = 0.9144;
    /**
     * Factor to convert metres into metres
     */
    public final static double METRES = 1.0;
    /**
     * Factor to convert inches into centimetres
     */
    public final static double INCHES = 0.0254;
    /**
     * Factor to convert centimetres into centimetres
     */
    public final static double CENTIMETRES = 0.01;

}
