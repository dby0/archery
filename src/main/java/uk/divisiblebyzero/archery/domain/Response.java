package uk.divisiblebyzero.archery.domain;

/**
 * Created by: Matthew Smalley
 * Date: 22/01/2016
 */
public class Response {
    private String content;

    public Response(String content) {
        this.content = content;
    }

    public String getContent() {

        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
