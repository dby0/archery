package uk.divisiblebyzero.archery.domain;

/**
 * Created by: Matthew Smalley
 * Date: 05/09/2016
 */
public enum Face {

    CM122(122d),
    CM80(80d),
    CM60(60d),
    IN16(16d * 2.54);

    private Face(double size) {
        this.size = size;
    }

    private double size;

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }
}
