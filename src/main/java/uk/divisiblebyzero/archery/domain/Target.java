package uk.divisiblebyzero.archery.domain;

/**
 * Created by: Matthew Smalley
 * Date: 28/03/2016
 */
public enum Target {
    TARGET_80_CM(80 * Conversions.CENTIMETRES, 8 * Conversions.CENTIMETRES),
    TARGET_60_CM(60 * Conversions.CENTIMETRES, 6 * Conversions.CENTIMETRES),
    TARGET_16_INCHES(16 * Conversions.INCHES, 3.2 * Conversions.INCHES);


    private double targetWidth;
    private double spacing;

    Target(double targetWidth, double spacing) {
        this.targetWidth = targetWidth;
        this.spacing = spacing;
    }

    public double getTargetWidth() {
        return targetWidth;
    }

    public double getSpacing() {
        return spacing;
    }
}
