package uk.divisiblebyzero.archery.domain;

/**
 * Created by: Matthew Smalley
 * Date: 17/11/2015
 */
public enum BowType {
    RECURVE, COMPOUND, BAREBOW, LONGBOW;
}
