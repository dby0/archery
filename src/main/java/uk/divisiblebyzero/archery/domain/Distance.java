package uk.divisiblebyzero.archery.domain;

/**
 * Created by: Matthew Smalley
 * Date: 05/09/2016
 */
public class Distance {

    private final static double metresPerYard = 0.9144d;
    private Face face;
    private double rangeInMetres;
    private int arrows;

    public static Distance createDistanceInMetres(Face face, double rangeInMetres, int arrows) {
        Distance d = new Distance();
        d.face = face;
        d.rangeInMetres = rangeInMetres;
        d.arrows = arrows;
        return d;
    }

    public static Distance createDistanceInYards(Face face, double rangeInYards, int arrows) {
        return createDistanceInMetres(face, rangeInYards * metresPerYard, arrows);
    }


    public Face getFace() {
        return face;
    }

    public void setFace(Face face) {
        this.face = face;
    }

    public double getRangeInMetres() {
        return rangeInMetres;
    }

    public void setRangeInMetres(double rangeInMetres) {
        this.rangeInMetres = rangeInMetres;
    }

    public double getRangeInYards() {
        return rangeInMetres / metresPerYard;
    }

    public void setRangeInYards(double rangeInYards) {
        this.rangeInMetres = rangeInYards * metresPerYard;
    }

    public int getArrows() {
        return arrows;
    }

    public void setArrows(int arrows) {
        this.arrows = arrows;
    }
}
