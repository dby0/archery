package uk.divisiblebyzero.archery.domain;

import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 17/11/2015
 */
public class End {
    private List<Integer> scores;
    private int endTotal;

    public List<Integer> getScores() {
        return scores;
    }

    public void setScores(List<Integer> scores) {
        this.scores = scores;
    }

    public int getEndTotal() {
        return endTotal;
    }

    public void setEndTotal(int endTotal) {
        this.endTotal = endTotal;
    }
}
