package uk.divisiblebyzero.archery.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by: Matthew Smalley
 * Date: 23/01/2016
 */
public class ArcheryEvent {
    private Date date;
    private Set<ArcheryRound> rounds = new HashSet<>();

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Set<ArcheryRound> getRounds() {
        return rounds;
    }

    public void setRounds(Set<ArcheryRound> rounds) {
        this.rounds = rounds;
    }

    @Override
    public String toString() {
        return "ArcheryEvent{" +
                "date=" + date +
                ", rounds=" + rounds +
                '}';
    }
}
