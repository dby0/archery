package uk.divisiblebyzero.archery.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedBy;

/**
 * Created by: Matthew Smalley
 * Date: 11/09/2016
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SightMark {
    private String id;
    @CreatedBy
    private String userId;
    private long timestamp;
    private int distance;
    private double sightMark;
    private SightMarkUnits units;

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public double getSightMark() {
        return sightMark;
    }

    public void setSightMark(double sightMark) {
        this.sightMark = sightMark;
    }

    public SightMarkUnits getUnits() {
        return units;
    }

    public void setUnits(SightMarkUnits units) {
        this.units = units;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "SightMark{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", timestamp=" + timestamp +
                ", distance=" + distance +
                ", sightMark=" + sightMark +
                ", units=" + units +
                '}';
    }
}
