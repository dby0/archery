package uk.divisiblebyzero.archery.domain;

import org.springframework.data.annotation.Id;

import java.util.Date;
import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 17/11/2015
 */
public class ArcheryRound {
    @Id
    private String id;
    private String archer;
    private Date date;
    private String time;
    private BowType bowType;
    private boolean indoor;
    private String roundType;
    private int totalScore;
    private String location;
    private List<Dozen> dozens;
    private int handicap;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getArcher() {
        return archer;
    }

    public void setArcher(String archer) {
        this.archer = archer;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public BowType getBowType() {
        return bowType;
    }

    public void setBowType(BowType bowType) {
        this.bowType = bowType;
    }

    public boolean isIndoor() {
        return indoor;
    }

    public void setIndoor(boolean indoor) {
        this.indoor = indoor;
    }

    public String getRoundType() {
        return roundType;
    }

    public void setRoundType(String roundType) {
        this.roundType = roundType;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    public List<Dozen> getDozens() {
        return dozens;
    }

    public void setDozens(List<Dozen> dozens) {
        this.dozens = dozens;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getHandicap() {
        return handicap;
    }

    public void setHandicap(int handicap) {
        this.handicap = handicap;
    }
}
