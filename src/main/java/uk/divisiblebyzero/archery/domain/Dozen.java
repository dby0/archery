package uk.divisiblebyzero.archery.domain;

/**
 * Created by: Matthew Smalley
 * Date: 17/11/2015
 */
public class Dozen {
    private End end0;
    private End end1;
    private int hits;
    private int score;
    private int golds;
    private int runningTotal;

    public End getEnd0() {
        return end0;
    }

    public void setEnd0(End end0) {
        this.end0 = end0;
    }

    public End getEnd1() {
        return end1;
    }

    public void setEnd1(End end1) {
        this.end1 = end1;
    }

    public int getHits() {
        return hits;
    }

    public void setHits(int hits) {
        this.hits = hits;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getGolds() {
        return golds;
    }

    public void setGolds(int golds) {
        this.golds = golds;
    }

    public int getRunningTotal() {
        return runningTotal;
    }

    public void setRunningTotal(int runningTotal) {
        this.runningTotal = runningTotal;
    }
}
