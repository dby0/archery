package uk.divisiblebyzero.archery.domain;

/**
 * Created by: Matthew Smalley
 * Date: 11/09/2016
 */
public enum SightMarkUnits {
    YARDS, METRES
}

