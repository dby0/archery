package uk.divisiblebyzero.archery.domain;

import java.util.Set;

/**
 * Created by: Matthew Smalley
 * Date: 16/01/2016
 */
public class User {
    private String email;
    private Set<String> roles;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", roles=" + roles +
                '}';
    }
}