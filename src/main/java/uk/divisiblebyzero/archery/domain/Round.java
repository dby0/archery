package uk.divisiblebyzero.archery.domain;

import java.util.Arrays;
import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 05/09/2016
 */
public enum Round {

    NATIONAL("National", Arrays.asList(Distance.createDistanceInYards(Face.CM122, 60d, 48), Distance.createDistanceInYards(Face.CM122, 50d, 24)), ScoringStyle.IMPERIAL),
    PORTSMOUTH("Portsmouth", Arrays.asList(Distance.createDistanceInYards(Face.CM60, 20d, 60)), ScoringStyle.METRIC),
    FROSTBITE("Frostbite", Arrays.asList(Distance.createDistanceInMetres(Face.CM80, 30d, 36)), ScoringStyle.METRIC),
    FITA18("FITA18", Arrays.asList(Distance.createDistanceInMetres(Face.CM60, 18d, 60)), ScoringStyle.METRIC),
    WORCESTER("Worcester", Arrays.asList(Distance.createDistanceInYards(Face.IN16, 20d, 50)), ScoringStyle.WORCESTER),
    DEFAULT("Unknown", Arrays.asList(Distance.createDistanceInYards(Face.CM122, 100d, 60)), ScoringStyle.METRIC);

    private Round(String roundName, List<Distance> distances, ScoringStyle scoringStyle) {
        this.roundName = roundName;
        this.distances = distances;
        this.scoringStyle = scoringStyle;
    }

    private String roundName;
    private List<Distance> distances;
    private ScoringStyle scoringStyle;

    public String getRoundName() {
        return roundName;
    }

    public void setRoundName(String roundName) {
        this.roundName = roundName;
    }

    public List<Distance> getDistances() {
        return distances;
    }

    public void setDistances(List<Distance> distances) {
        this.distances = distances;
    }

    public ScoringStyle getScoringStyle() {
        return scoringStyle;
    }

    public void setScoringStyle(ScoringStyle scoringStyle) {
        this.scoringStyle = scoringStyle;
    }
}
