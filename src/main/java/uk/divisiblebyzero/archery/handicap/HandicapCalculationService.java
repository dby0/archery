package uk.divisiblebyzero.archery.handicap;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.divisiblebyzero.archery.domain.Distance;
import uk.divisiblebyzero.archery.domain.Round;
import uk.divisiblebyzero.archery.domain.ScoringStyle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by: Matthew Smalley
 * Date: 05/09/2016
 * <p/>
 * Interpreted from: http://www.roystonarchery.org/new/wp-content/uploads/2013/09/Graduated-Handicap-Tables.pdf?e632e9
 */
public class HandicapCalculationService {

    private final static Logger LOGGER = LoggerFactory.getLogger(HandicapCalculationService.class);

    private class HandicapEntry {
        int handicap;
        int score;

        @Override
        public String toString() {
            return "HandicapEntry{" +
                    "handicap=" + handicap +
                    ", score=" + score +
                    '}';
        }
    }

    private Map<Round, List<HandicapEntry>> handicapTables = new HashMap<>();

    public int getHandicapForScore(Round round, int score) {
        LOGGER.info("Looking for " + round.getRoundName() + " handicap for score: " + score);

        if (round == Round.DEFAULT) {
            return 100;
        }

        List<HandicapEntry> handicapTable = getHandicapTable(round);
        Optional<HandicapEntry> value = handicapTable
                .stream()
                .filter(a -> a.score <= score)
                .findFirst();
        if (value.isPresent()) {
            return value.get().handicap;
        } else {
            return 100;
        }


    }

    private double getRootMeanSquare(int handicap, Distance distance) {
        double rangeInMetres = distance.getRangeInMetres();
        return 100.0D * rangeInMetres * Math.pow(1.036D, 12.9D + handicap) * 5.0D * Math.pow(10.0D, -4.0D) * (1.0D + 1.429D * Math.pow(10.0D, -6.0D) * Math.pow(1.07D, 4.3D + handicap) * Math.pow(rangeInMetres, 2.0D));
    }

    private double yardsToMeters(double yards) {
        return 0.9144D * yards;
    }

    private double calculateScoreForHandicap(int roundScore, Distance distance, ScoringStyle scoringStyle) {
        double d = 0.0D;
        double rootMeanSquare = getRootMeanSquare(roundScore, distance);
        double averageScore = d;

        if (scoringStyle == ScoringStyle.IMPERIAL) {
            for (int i = 1; i <= 4; i++) {
                averageScore += Math.exp(-Math.pow(i * distance.getFace().getSize() / 10.0D + 0.357D, 2.0D) / Math.pow(rootMeanSquare, 2.0D));
            }
            return 9.0D - 2.0D * averageScore - Math.exp(-Math.pow(distance.getFace().getSize() / 2.0D + 0.357D, 2.0D) / Math.pow(rootMeanSquare, 2.0D));
        } else if (scoringStyle == ScoringStyle.METRIC) {
            for (int i = 1; i <= 10; i++) {
                averageScore += Math.exp(-Math.pow(i * distance.getFace().getSize() / 20.0D + 0.357D, 2.0D) / Math.pow(rootMeanSquare, 2.0D));
            }
            return 10.0D - averageScore;
        } else if (scoringStyle == ScoringStyle.WORCESTER) {
            for (int i = 1; i <= 5; i++) {
                averageScore += Math.exp(-Math.pow(i * distance.getFace().getSize() / 10.0D + 0.357D, 2.0D) / Math.pow(rootMeanSquare, 2.0D));
            }
            return 5.0D - averageScore;
        } else {
            System.err.println("Error - unknown scoring style");
            return 0d;
        }
    }

    protected List<HandicapEntry> getHandicapTable(Round round) {
        if (handicapTables.containsKey(round)) {
            return handicapTables.get(round);
        }
        List<HandicapEntry> handicapTable = new ArrayList<>();
        for (int i = 0; i < 101; i++) {
            HandicapEntry entry = new HandicapEntry();
            entry.handicap = i;
            entry.score = calculateScoreForHandicap(i, round);
            handicapTable.add(entry);
        }
        handicapTables.put(round, handicapTable);
        return handicapTable;
    }

    public int calculateScoreForHandicap(int handicap, Round round) {
        /* National round:
        48x 60y, 122cm face
        24x 50y, 122cm face
         */

        double runningTotal = 0D;
        for (Distance distance : round.getDistances()) {
            runningTotal += distance.getArrows() * calculateScoreForHandicap(handicap, distance, round.getScoringStyle());
        }
        return (int) Math.round(runningTotal);
    }
}
