package uk.divisiblebyzero.archery.service;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uk.divisiblebyzero.archery.domain.SightMark;

/**
 * Created by: Matthew Smalley
 * Date: 17/01/2016
 */
@RepositoryRestResource(collectionResourceRel = "sightMarks", path = "sightMarks")
public interface SightMarkRepository extends MongoRepository<SightMark, Long> {


}