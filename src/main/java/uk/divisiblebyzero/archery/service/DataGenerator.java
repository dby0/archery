package uk.divisiblebyzero.archery.service;

import org.springframework.stereotype.Service;
import uk.divisiblebyzero.archery.domain.ArcheryRound;
import uk.divisiblebyzero.archery.domain.BowType;
import uk.divisiblebyzero.archery.domain.Dozen;
import uk.divisiblebyzero.archery.domain.End;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by: Matthew Smalley
 * Date: 22/11/2015
 */
@Service
public class DataGenerator {


    private ArcheryRound sampleArcheryRound;

    public ArcheryRound getSampleArcheryRound() {

        if (sampleArcheryRound == null) {

            sampleArcheryRound = new ArcheryRound();
            sampleArcheryRound.setArcher("The Green Arrow");
            sampleArcheryRound.setBowType(BowType.RECURVE);
            sampleArcheryRound.setDate(new Date());
            sampleArcheryRound.setTime("12:00");
            sampleArcheryRound.setIndoor(false);
            sampleArcheryRound.setRoundType("FROSTBITE");
            sampleArcheryRound.setId("blah");


            End end = new End();
            end.setScores(Arrays.asList(new Integer[]{9, 9, 9, 8, 7, 6}));


            sampleArcheryRound.setDozens(new ArrayList<>());

            for (int i = 0; i < 3; i++) {
                Dozen dozen = new Dozen();
                dozen.setEnd0(end);
                dozen.setEnd1(end);
                sampleArcheryRound.getDozens().add(dozen);
            }
        }

        return sampleArcheryRound;

    }

    @Resource
    private ArcheryRoundRepository archeryRoundRepository;

    public void createFakeData() {
        createArcheryRound();
    }

    public void createArcheryRound() {
        archeryRoundRepository.save(getSampleArcheryRound());

    }
}
