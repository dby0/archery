package uk.divisiblebyzero.archery.service;

import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;
import uk.divisiblebyzero.archery.domain.ArcheryRound;

import javax.annotation.Resource;

/**
 * Created by: Matthew Smalley
 * Date: 22/01/2016
 */
@RepositoryEventHandler(ArcheryRound.class)
@Component
public class ArcheryRoundEventHandler {
    @Resource
    private ArcheryRoundService archeryRoundService;

    public ArcheryRoundEventHandler() {
        System.out.println("Instantiating...");
    }

    @HandleBeforeSave
    public void handleArcheryRoundSave(ArcheryRound archeryRound) {
        System.out.println("Hook worked (save)");
        System.out.println("Before:" + archeryRound);
        archeryRoundService.calculateTotals(archeryRound);
        System.out.println("After:" + archeryRound);
    }

    @HandleBeforeCreate
    public void handleArcheryRoundCreate(ArcheryRound archeryRound) {
        System.out.println("Hook worked (create)");
        System.out.println("Before:" + archeryRound);
        archeryRoundService.calculateTotals(archeryRound);
        System.out.println("After:" + archeryRound);
    }

}
