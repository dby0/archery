package uk.divisiblebyzero.archery.service;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uk.divisiblebyzero.archery.domain.ArcheryRound;
import uk.divisiblebyzero.archery.domain.Dozen;
import uk.divisiblebyzero.archery.domain.Response;
import uk.divisiblebyzero.archery.domain.Round;
import uk.divisiblebyzero.archery.handicap.HandicapCalculationService;

import javax.annotation.Resource;

/**
 * Created by: Matthew Smalley
 * Date: 17/11/2015
 */
@RestController
public class ArcheryRoundService {
    private final static double AQ_SCALING_FACTOR = 0.004;
    private HandicapCalculationService handicapCalculationService = new HandicapCalculationService();

    @RequestMapping("/recalculate")
    public Response recalculateAll() {
        for (ArcheryRound archeryRound : archeryRoundRepository.findAll()) {
            calculateTotals(archeryRound);
            archeryRoundRepository.save(archeryRound);
        }

        return new Response("OK");
    }


    @Resource
    private ArcheryRoundRepository archeryRoundRepository;

    public void calculateTotals(ArcheryRound archeryRound) {
        Round round;
        try {
            round = Round.valueOf(Round.class, archeryRound.getRoundType());
        } catch (IllegalArgumentException e) {
            round = Round.DEFAULT;
        }

        int runningTotal = 0;
        if (archeryRound.getDozens() != null) {
            for (Dozen dozen : archeryRound.getDozens()) {
                int dozenTotal = 0;
                int dozenHits = 0;
                int dozenGolds = 0;
                int end0Total = 0;
                int end1Total = 0;

                if (dozen.getEnd0() != null) {
                    for (int arrowScore : dozen.getEnd0().getScores()) {
                        end0Total += arrowScore;
                        if (arrowScore == round.getScoringStyle().getGoldScore()) {
                            dozenGolds++;
                        }
                        if (arrowScore != 0) {
                            dozenHits++;
                        }
                    }
                    dozen.getEnd0().setEndTotal(end0Total);
                }

                if (dozen.getEnd1() != null) {
                    for (int arrowScore : dozen.getEnd1().getScores()) {
                        end1Total += arrowScore;
                        if (arrowScore == 10) {
                            dozenGolds++;
                        }
                        if (arrowScore != 0) {
                            dozenHits++;
                        }
                    }
                    dozen.getEnd1().setEndTotal(end1Total);
                }

                dozenTotal = end0Total + end1Total;
                runningTotal += dozenTotal;

                dozen.setRunningTotal(runningTotal);
                dozen.setScore(dozenTotal);
                dozen.setHits(dozenHits);
                dozen.setGolds(dozenGolds);

            }
        }
        archeryRound.setTotalScore(runningTotal);
        archeryRound.setHandicap(handicapCalculationService.getHandicapForScore(round, runningTotal));

    }
}
