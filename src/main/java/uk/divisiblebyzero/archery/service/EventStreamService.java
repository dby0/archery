package uk.divisiblebyzero.archery.service;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uk.divisiblebyzero.archery.domain.ArcheryEvent;
import uk.divisiblebyzero.archery.domain.ArcheryRound;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by: Matthew Smalley
 * Date: 23/01/2016
 */
@RestController
public class EventStreamService {

    @Resource
    private MongoTemplate mongoTemplate;

    @Resource
    private ArcheryRoundRepository archeryRoundRepository;

    /**
     * Would love to use mongo to do this aggregation, but I can't find a way of EITHER upgrading
     * OpenShift to Mongo 3.x (and using $$ROOT), OR using document push in Spring Mongo...
     *
     * @return
     */
    @RequestMapping("/events")
    public List<ArcheryEvent> getEvents() {

        // Ideal query is:
        // db.archeryRound.aggregate([{$match: {totalScore: {$ne: 0}}}, {$group: {_id: "$date", rounds: {$push: "$$ROOT"} }} ])

        Map<Date, ArcheryEvent> events = new HashMap<>();
        List<ArcheryRound> rounds = archeryRoundRepository.findAll();
        for (ArcheryRound round : rounds) {
            if (round.getDate() != null && round.getTotalScore() != 0) {
                if (!events.containsKey(round.getDate())) {
                    ArcheryEvent event = new ArcheryEvent();
                    event.setDate(round.getDate());
                    events.put(round.getDate(), event);
                }
                events.get(round.getDate()).getRounds().add(round);
            }
        }

        List<ArcheryEvent> eventsList = new ArrayList<>();
        eventsList.addAll(events.values());

        Collections.sort(eventsList, new Comparator<ArcheryEvent>() {
            @Override
            public int compare(ArcheryEvent o1, ArcheryEvent o2) {
                return o2.getDate().compareTo(o1.getDate());
            }
        });

        return eventsList;
/*
        Aggregation agg = newAggregation(
                group("date").push("{_id, archer}").as("rounds"),
                project("rounds").and("_id").as("date")
        );

        AggregationResults<ArcheryEvent> results = mongoTemplate.aggregate(agg, "archeryRound", ArcheryEvent.class);
        return results.getMappedResults();*/
    }
}
