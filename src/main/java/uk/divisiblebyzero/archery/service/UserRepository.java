package uk.divisiblebyzero.archery.service;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import uk.divisiblebyzero.archery.domain.User;

/**
 * Created by: Matthew Smalley
 * Date: 17/01/2016
 */
@Repository
public interface UserRepository extends MongoRepository<User, Long> {

    User findByEmail(String email);
}