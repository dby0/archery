package uk.divisiblebyzero.archery.service;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uk.divisiblebyzero.archery.domain.ArcheryRound;

import java.util.List;

/**
 * Created by: Matthew Smalley
 * Date: 16/01/2016
 */
@RepositoryRestResource(collectionResourceRel = "archeryRounds", path = "archeryRounds")
public interface ArcheryRoundRepository extends MongoRepository<ArcheryRound, String> {
    List<ArcheryRound> findByArcher(@Param("archer") String archer);

    List<ArcheryRound> findByArcherAndRoundTypeOrderByDateDesc(@Param("archer") String archer, @Param("roundType") String roundType);

    List<ArcheryRound> findByArcherOrderByDateDesc(String archer);
}
