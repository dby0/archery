package uk.divisiblebyzero.archery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by: Matthew Smalley
 * Date: 15/01/2016
 */
@SpringBootApplication
public class ArcheryApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(ArcheryApplication.class, args);
    }

}
